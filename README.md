# rez-package-template

Simple [Rez](https://github.com/nerdvegas/rez) package template with comments.

For more information, check the Wiki : https://github.com/nerdvegas/rez/wiki/Package-Definition-Guide

## Installation

Just clone the repo : 

```bash
git clone https://gitlab.com/artfx-pipeline/rez-package-template.git
```

## Usage

Copy the `package.py` file into your Rez package folder and modify its properties.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
